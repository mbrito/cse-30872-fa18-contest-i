#! /usr/bin/env python3

import sys
import itertools


def getDistance(l):
    INFINITY = float("inf")
    minDist = INFINITY
    for i in range(len(l) - 1):
        if l[i+1] - l[i] < minDist:
            minDist = l[i+1] - l[i]
            if minDist == 1:
                return 0
    return minDist - 1


def getMinD(potList, flowers):
    INFINITY = float("inf")
    minDist = -1 * INFINITY
    potList = sorted(potList)
    #Assume that the pots at the beginning and end will always be used.
    #Makes sense since trying to maximize the minimum distance between two pots.
    for i in itertools.combinations(potList[1:-1], flowers - 2):
        i = [potList[0]] + list(i) + [potList[-1]]
        a = getDistance(i)
        if a > minDist:
            minDist = a
            if minDist == 1:
                return 1
    return minDist

if __name__ == "__main__":


    first = True
    second = False
    for line in sys.stdin:
        if first:
            count = 0
            l = line.rstrip().split()
            pots = int(l[0])
            flowers = int(l[1])
            first = False
            potList = []
        else:
            potList.append(int(line.rstrip()))
            count += 1
            if count == pots:
                first = True
                print(getMinD(potList, flowers))


