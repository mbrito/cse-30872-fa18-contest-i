import java.util.*;
import java.util.stream.*;

public class program {

	public static void main(String args[]){
		Scanner sin = new Scanner(System.in);
		while(sin.hasNextLine()){
			String line[] = sin.nextLine().split(" ");
			if(line.length == 0) continue;
			int n = Integer.parseInt(line[0]);
			int swaps = Integer.parseInt(line[1]);
			
			String sarr[] = sin.nextLine().split(" ");
			int arr[] = new int[n];
			HashMap<Integer, Integer> positions = new HashMap<>();
			for(int i = 0; i < n; i++){
				int num = Integer.parseInt(sarr[i]);
				arr[i] = num;
				positions.put(num, i);
			}
			for(int i = n; i > 0; i--){
				if(positions.get(i) != n - i){
					int toSwap = arr[n - i];
					int oldPos = positions.get(i);
					positions.put(toSwap, oldPos);
					positions.put(i, n - i);
					arr[n - i] = i;
					arr[oldPos] = toSwap;
					swaps--;
					if(swaps == 0) break;
				}
			}
			for(int i = 0; i < n; i++){
				if(i != 0) System.out.print(" ");
				System.out.print(arr[i]);
			}
			System.out.println();
		}
	}
}
