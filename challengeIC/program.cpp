/*#include <iostream>
#include <string>
using namespace std;

int numbers_swapped(string, string, int);

int main(int argc, char* argv[]){
	int N, K;
	while(cin >> N){
		cin >> K;
		int max = 0;
		string original;
		for (int i=0; i<N; i++)
			original.append(to_string(i));
		string current = original;
		do {
			if(stoi(current) > max && numbers_swapped(original, current, N) <= K)
				max = stoi(current);
		while (next_permutation(current.begin(), current.end());
		cout << max;
	}
	return 0;
}

int numbers_swapped (string original, string current, int N){
	int swaps;
	for(int i=0; i<N; i++){
		if (current[i] != original[i])
			swaps++;
	}
	return swaps/2;
}*/
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

void limited_swap(vector<int> &current, int, int);

int main(int argc, char* argv[]){
        int N, K;
        while(cin >> N){
                cin >> K;
                int max = 0;
                vector<int> original;
                int next;
                for (int i=1; i<N+1; i++){
                        cin >> next;
                        original.push_back(next);
                }
                vector<int> current = original;
                limited_swap(current, N, K);
                for (int i=0; i<current.size(); i++)
                        cout << current[i];
                cout << endl;
        }
        return 0;
}

void limited_swap (vector<int> &current, int N, int K){
        vector<int> temp;
        while (K>0 && current.size() > 0){
                int max_i = 0, max = 0;//, second_i = 0, second=0;
                //temp.clear();
                for (int i=0; i<current.size(); i++){
                        if(current[i] > max){
                                //second_i = max_i;
                                //second = max;
                                max_i = i;
                                max = current[i];
                        }
                }
                swap(current[0], current[max_i]);
                temp.push_back(current[0]);
                current.erase(current.begin());
                K--;
        }
        if (current.size() > 0)
                for (int i=0; i<current.size(); i++)
                        temp.push_back(current[i]);
        current = temp;
}
