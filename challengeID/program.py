#! /usr/bin/env python3
import sys


lookup = {}

def canWork(potList, flowers, dist):
    i = 1
    count = 1
    last = potList[0]
    while count < flowers:
        if i >= len(potList):
            return False
        if (potList[i] - last) >= dist:
            count += 1
            last = potList[i]
        i += 1
    return True


def getAnswer(potList, flowers):
    optDist = ((potList[-1] - potList[0]) // (flowers - 1))
   

    left = 1
    right = optDist
    i = (left + right) // 2
    while left <= right:
        works = canWork(potList, flowers, i) 
        if works and not canWork(potList, flowers, i + 1):
            return i
        if works:
            left = i + 1
        else:
            right = i
        i = (left + right) // 2
 

if __name__ == "__main__":


    first = True
    for line in sys.stdin:
        if first:
            count = 0
            l = line.rstrip().split()
            pots = int(l[0])
            flowers = int(l[1])
            first = False
            potList = []
        else:
            potList.append(int(line.rstrip()))
            count += 1
            if count == pots:
                first = True
                potList.sort()
                print(int(getAnswer(potList, flowers)) - 1)

