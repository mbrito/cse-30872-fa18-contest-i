import java.util.*;

public class program {
	public static void main(String args[]){
		Scanner sin = new Scanner(System.in);
		while(sin.hasNextLine()){
			String line[] = sin.nextLine().split(" ");
			ArrayList<Integer> list = new ArrayList<>();
			for(String s : line){
				list.add(Integer.parseInt(s));
			}
			int count = 0;
			int max = 0;
			Collections.reverse(list);
			for(int i : list){
				if(i > max){
					count++;
					max = i;
				}
			}
			System.out.println(count);
		}
	}
}
