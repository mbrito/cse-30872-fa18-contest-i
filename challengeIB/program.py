#! /usr/bin/env python3

import sys

def isIsomorphic(a, b):
    D = {}
    shorterWord = a if len(a) < len(b) else b
    longerWord = a if shorterWord == b else b

   # print(shorterWord)
   # print(longerWord)

    for i, c in enumerate(shorterWord):
        if c not in D:
            D[c] = longerWord[i]
    #        print(c + " = " + D[c])
        elif D[c] != longerWord[i]:
            return False

    values = set()
    for c in D:
        if D[c] in values:
            return False
        values.add(D[c])

    return True


if __name__ == "__main__":
    for line in sys.stdin:
        words = line.split()
        if isIsomorphic(words[0], words[1]):
            print("Isomorphic")
        else:
            print("Not Isomorphic")
