#! /usr/bin/env python3

import itertools
import sys


def padZeros(s, length):
    ret = ""
    ret += s
    while len(ret) < length:
        ret = "0" + ret
    return ret


def countOnes(a):
    ret = 0
    for i in a:
        if i == "1":
            ret += 1
    return ret

def printPerm(n, k):
    for i in range(2 ** n):
        s = padZeros(str(bin(i))[2:], n)
        if countOnes(s) == k:
            print(s)

if __name__ == "__main__":
    first = True
    for line in sys.stdin:
        if not first:
            print("")
        first = False
        s = line.rstrip().split()
        n = s[0]
        k = s[1]
        printPerm(int(n), int(k))


