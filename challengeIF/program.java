import java.util.*;
import java.util.stream.*;

public class program {
	public static void main(String args[]){
		Scanner sin = new Scanner(System.in);
		while(sin.hasNextLine()){
			int num = Integer.parseInt(sin.nextLine());
			List<String> combs = new ArrayList<>(getCombinations(num));
			Collections.sort(combs);
			String plural = "s";
			String verb = "are";
			if(combs.size() == 1){
				plural = "";
				verb = "is";
			}
			System.out.format("There %s %d way%s to achieve a score of %d:\n", verb, combs.size(), plural, num);
			for(String s : combs){
				System.out.println(spaced(s));
			}
		}
	}
	public static HashMap<Integer, Set<String>> waysMap = new HashMap<>();
	public static Set<String> getCombinations(int num){
		Set<String> ways = new HashSet<>();
		if(waysMap.get(num) != null) return waysMap.get(num);
		if(num == 2 || num == 3 || num == 7){
			ways.add(num + "");
		}
		if(num > 2){
			ways.addAll(map(2, getCombinations(num - 2)));
		}
		if(num > 3){
			ways.addAll(map(3, getCombinations(num - 3)));
		}
		if(num > 7){
			ways.addAll(map(7, getCombinations(num - 7)));
		}
		waysMap.put(num, ways);
		return ways;
	}
	public static Set<String> map(int num, Set<String> strs){
		return strs.stream().map(str -> num + "" + str).map(str -> {
			char[] chars = str.toCharArray();
			Arrays.sort(chars);
			String s = "";
			for(char c : chars){
				s += c;
			}
			return s;
		}).collect(Collectors.toSet());
	}

	public static String spaced(String str){
		String s = "";
		char[] chars = str.toCharArray();
		int first = 0;
		for(char c : chars){
			if(first++ != 0) s+= " ";
			s += c;
		}
		return s;
	}
}
