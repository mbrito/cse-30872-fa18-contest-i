#! /usr/bin/env python3

import sys

def performSwap(a, s, n, l):
    i = 0
    while i < l and s[i] == a[i]:
        i += 1
    if i == l:
        return a
    target = s[i]
    j = l - 1
    while a[j] != target:
        j -= 1
    temp = a[i]
    a[i] = a[j]
    a[j] = temp
    return a

def performNSwaps(arr, n):
    s = sorted(arr, reverse=True)
    l = len(arr)
    for _ in range(n):
        arr = performSwap(arr, s, n, l)
    return arr



if __name__ == "__main__":

    first = True
    for line in sys.stdin:
        if first:
            numSwaps = int(line.rstrip().split()[1])
            first = False
        else:
            nums = [int(i) for i in line.rstrip().split()]
            nums = performNSwaps(nums, numSwaps)
            print(' '.join([str(n) for n in nums]))
            first = True
